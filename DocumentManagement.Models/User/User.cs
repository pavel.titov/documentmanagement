﻿using System.ComponentModel.DataAnnotations;

namespace DocumentManagement.Models
{
    public class User : BaseModel
    {
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }
    }
}
