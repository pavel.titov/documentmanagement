﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DocumentManagement.Models
{
    public class DocumentStage : BaseModel
    {
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [Required]
        [MaxLength(255)]
        public string Description { get; set; }

        public ICollection<DocumentRoute> Routes { get; set; }

        public List<DocumentRouteStage> RouteStages { get; set; }
    }
}
