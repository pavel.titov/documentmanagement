﻿using System.ComponentModel.DataAnnotations;

namespace DocumentManagement.Models
{
    public class DocumentTemplate : BaseModel
    {
        public int DocumentTypeId { get; set; }
        public DocumentType DocumentType { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

        [Required]
        public string Template { get; set; }

        public int RouteId { get; set; }
        public DocumentRoute Route { get; set; }
    }
}