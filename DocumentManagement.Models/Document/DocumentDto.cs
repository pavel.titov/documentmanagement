﻿using System;
using System.Linq;

namespace DocumentManagement.Models
{
    public class DocumentDto
    {
        public int Id { get; set; }

        public int TemplateId { get; set; }

        public string Template { get; set; }

        public string Creator { get; set; }

        public string Stage { get; set; }

        public DateTime DateCreate { get; set; }

        public DateTime? DateEdit { get; set; }

        public static DocumentDto FromDocument(Document document)
        {
            return new DocumentDto()
            {
                Id = document.Id,
                TemplateId = document.DocumentTemplateId,
                Template = document.DocumentTemplate.Description,
                Creator = document.Creator.Name,
                Stage = document.History.OrderByDescending(h => h.Id).First().Stage.Description,
                DateCreate = document.DateCreate,
                DateEdit = document.DateEdit
            };
        }
    }
}
