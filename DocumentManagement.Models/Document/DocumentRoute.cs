﻿using System.Collections.Generic;

namespace DocumentManagement.Models
{
    public class DocumentRoute : BaseModel
    {
        public ICollection<DocumentStage> Stages { get; set; }
        public List<DocumentRouteStage> RouteStages { get; set; }
    }
}
