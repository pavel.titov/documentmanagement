﻿using System.Collections.Generic;

namespace DocumentManagement.Models
{
    public class Document : BaseModel
    {
        public int DocumentTemplateId { get; set; }
        public DocumentTemplate DocumentTemplate { get; set; }

        public int CreatorId { get; set; }
        public User Creator { get; set; }

        public List<DocumentTransition> History { get; set; }
    }
}
