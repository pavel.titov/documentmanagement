﻿using System.ComponentModel.DataAnnotations;

namespace DocumentManagement.Models
{
    public class DocumentType : BaseModel
    {
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }
    }
}
