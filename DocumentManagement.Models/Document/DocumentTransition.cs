﻿namespace DocumentManagement.Models
{
    public class DocumentTransition : BaseModel
    {
        public int DocumentId { get; set; }
        public Document Document { get; set; }

        public int StageId { get; set; }
        public DocumentStage Stage { get; set; }
    }
}