﻿namespace DocumentManagement.Models
{
    public class DocumentRouteStage : BaseModel
    {
        public int RouteId { get; set; }
        public DocumentRoute Route { get; set; }

        public int StageId { get; set; }
        public DocumentStage Stage { get; set; }
    }
}
