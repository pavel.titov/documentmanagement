﻿using System;

namespace DocumentManagement.Models
{
    public interface IDateTrackable
    {
        DateTime DateCreate { get; set; }

        DateTime? DateEdit { get; set; }
    }
}