﻿namespace DocumentManagement.Models
{
    public enum Stage : int
    {
        Draft = 1,
        Registered = 2,
        Pending = 3,
        Confirmed = 4,
        Complete = 5,
        Canceled = 6,
        Archived = 7
    }
}
