﻿using Avro.Specific;

namespace DocumentManagement.Kafka
{
    public abstract class KafkaFactory<TKey, TEvent> where TEvent : ISpecificRecord
    {
        public abstract IKafkaConsumer<TKey, TEvent> CreateConsumer();
        public abstract IKafkaProducer<TKey, TEvent> CreateProducer();
    }
}
