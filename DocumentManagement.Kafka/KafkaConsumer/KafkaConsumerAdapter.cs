﻿using Avro.Specific;
using Confluent.Kafka;
using System;
using System.Threading;

namespace DocumentManagement.Kafka
{
    public class KafkaConsumerAdapter<TKey, TEvent> : IKafkaConsumer<TKey, TEvent> where TEvent : ISpecificRecord
    {
        public Exception Exception { get; private set; }

        private readonly IConsumer<TKey, TEvent> _consumer;
        private bool disposedValue;

        public KafkaConsumerAdapter(IConsumer<TKey, TEvent> consumer, Exception exception)
        {
            _consumer = consumer;
            Exception = exception;
        }

        public void Subscribe(string topic)
        {
            _consumer.Subscribe(topic);
        }

        public void Unsubscribe()
        {
            _consumer.Unsubscribe();
        }

        public ConsumeResult<TKey, TEvent> Consume(CancellationToken token)
        {
            var result = _consumer.Consume(token);
            Exception = null;
            return result;
        }

        public void StoreOffset(TopicPartitionOffset offset)
        {
            _consumer.StoreOffset(offset);
        }

        public void Close()
        {
            _consumer.Close();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _consumer.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
