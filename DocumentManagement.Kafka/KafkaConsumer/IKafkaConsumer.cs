﻿using Avro.Specific;
using Confluent.Kafka;
using System;
using System.Threading;

namespace DocumentManagement.Kafka
{
    public interface IKafkaConsumer<TKey, TEvent> : IDisposable where TEvent : ISpecificRecord
    {
        Exception Exception { get; }

        void Subscribe(string topic);
        void Unsubscribe();
        ConsumeResult<TKey, TEvent> Consume(CancellationToken token);
        void StoreOffset(TopicPartitionOffset offset);
        void Close();
    }
}