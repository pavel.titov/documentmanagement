﻿using Avro.Specific;
using Confluent.Kafka;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace DocumentManagement.Kafka
{
    public sealed class KafkaAccessor<TKey, TEvent> : BackgroundService, IKafkaAccessor<TKey, TEvent> where TEvent : ISpecificRecord
    {
        public event EventHandler Consumed;

        public CancellationTokenSource CancellationTokenSource { get; private set; } = new();

        private readonly ILogger<IKafkaAccessor<TKey, TEvent>> _logger;
        private readonly ISchemaRegistry _schemaRegistry;
        private readonly IKafkaConsumer<TKey, TEvent> _consumer;

        private IOptionsMonitor<KafkaOptions> Options { get; }

        private ConcurrentDictionary<long, TopicPartitionOffset> _offsets;
        private ConcurrentQueue<(long, TEvent)> _events;
        private ConcurrentDictionary<long, TEvent> _executingEvents;
        private int _eventCount = 0;
        private readonly ManualResetEventSlim _mre;

        private bool _enabled = true;
        private readonly object _lockEnable = new();

        public KafkaAccessor(
            ILogger<IKafkaAccessor<TKey, TEvent>> logger,
            IOptionsMonitor<KafkaOptions> options,
            ISchemaRegistry schemaRegistry,
            KafkaFactory<TKey, TEvent> factory
        )
        {
            _logger = logger;
            Options = options;
            Options.OnChange(Init);

            _schemaRegistry = schemaRegistry;

            _consumer = factory.CreateConsumer();

            _mre = new ManualResetEventSlim(true);

            Reset();
        }

        public TEvent GetEvent(out long id)
        {
            id = 0;

            if (_consumer.Exception != null)
            {
                throw _consumer.Exception;
            }

            if (_events.IsEmpty)
            {
                return (TEvent)(ISpecificRecord)null;
            }

            _events.TryDequeue(out var tuple);
            id = tuple.Item1;
            var @event = tuple.Item2;

            _executingEvents.TryAdd(id, @event);

            return @event;
        }

        public void UpdateStatus(long id)
        {
            if (!_enabled)
            {
                return;
            }

            _executingEvents.TryGetValue(id, out TEvent _);

            _offsets.TryRemove(id, out TopicPartitionOffset offset);

            var nextOffset = new Offset(offset.Offset.Value + 1);
            var nextTopicPartitionOffset = new TopicPartitionOffset(offset.TopicPartition, nextOffset);

            _consumer.StoreOffset(nextTopicPartitionOffset);

            _executingEvents.TryRemove(id, out TEvent _);

            _logger.LogDebug("Committed offset {Offset} to Kafka", offset.Offset.Value);
            Interlocked.Decrement(ref _eventCount);

            _mre.Set();
        }

        public override void Dispose()
        {
            _consumer.Close(); // Commit offsets and leave the group cleanly.
            _schemaRegistry.Dispose();

            base.Dispose();
        }

        protected override async Task ExecuteAsync(CancellationToken ct)
        {
            await Task.Yield();

            _logger.LogDebug("KafkaAccessor is started");

            ct.Register(() =>
            {
                _logger.LogDebug($"KafkaAccessor is stopping.");
                CancellationTokenSource.Cancel();
            });

            while (!ct.IsCancellationRequested)
            {
                try
                {
                    Consume();
                }
                catch (OperationCanceledException)
                {
                    _mre.Reset();
                    _consumer.Unsubscribe();
                    CancellationTokenSource = new CancellationTokenSource();
                }
                catch (ConsumeException e)
                {
                    _logger.LogError("Kafka consume error: {Reason}", e.Error.Reason);

                    if (e.Error.IsFatal)
                    {
                        // https://github.com/edenhill/librdkafka/blob/master/INTRODUCTION.md#fatal-consumer-errors
                        break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Unexpected Kafka consumer error: {e}");
                    break;
                }
            }

            _logger.LogDebug($"KafkaAccessor is stopped.");
        }

        private void Init(KafkaOptions options)
        {
            bool enable = options.Enabled;

            lock (_lockEnable)
            {
                if (enable == _enabled)
                {
                    return;
                }

                if (enable)
                {
                    Reset();
                }
                else
                {
                    CancellationTokenSource.Cancel();
                }

                _enabled = enable;
            }

            _logger.LogDebug("Enabled {enabled}", _enabled);
        }

        private void Reset()
        {
            _offsets = new ConcurrentDictionary<long, TopicPartitionOffset>();
            _events = new ConcurrentQueue<(long, TEvent)>();
            _executingEvents = new ConcurrentDictionary<long, TEvent>();
            _eventCount = 0;

            _mre.Set();

            _consumer.Subscribe(Options.CurrentValue.EventQueueTopic);
        }

        private void Consume()
        {
            _mre.Wait(CancellationTokenSource.Token);

            var consumeResult = _consumer.Consume(CancellationTokenSource.Token);

            _offsets.TryAdd(consumeResult.TopicPartitionOffset.Offset.Value, consumeResult.TopicPartitionOffset);

            _events.Enqueue((consumeResult.TopicPartitionOffset.Offset.Value, consumeResult.Message.Value));
            Interlocked.Increment(ref _eventCount);

            if (_eventCount >= Options.CurrentValue.Threads)
            {
                _mre.Reset();
            }

            Consumed?.Invoke(this, EventArgs.Empty);
        }
    }
}
