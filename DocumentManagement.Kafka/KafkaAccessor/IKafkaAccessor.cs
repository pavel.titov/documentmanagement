﻿using Microsoft.Extensions.Hosting;
using System;
using System.Threading;

namespace DocumentManagement.Kafka
{
    public interface IKafkaAccessor<TKey, TEvent> : IHostedService
    {
        event EventHandler Consumed;

        CancellationTokenSource CancellationTokenSource { get; }
        TEvent GetEvent(out long id);
        void UpdateStatus(long id);
    }
}