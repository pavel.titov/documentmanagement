﻿using Confluent.SchemaRegistry;
using Microsoft.Extensions.Options;
using System;

namespace DocumentManagement.Kafka
{
    public class SchemaRegistry : ISchemaRegistry
    {
        private bool disposedValue;

        public ISchemaRegistryClient Client { get; private set; }

        public SchemaRegistry(IOptionsMonitor<KafkaOptions> options)
        {
            var schemaRegistryConfig = options.CurrentValue.SchemaRegistry;
            Client = new CachedSchemaRegistryClient(schemaRegistryConfig);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Client.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
