﻿using Confluent.SchemaRegistry;
using System;

namespace DocumentManagement.Kafka
{
    public interface ISchemaRegistry : IDisposable
    {
        ISchemaRegistryClient Client { get; }
    }
}