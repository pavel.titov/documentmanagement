﻿using Avro.Specific;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DocumentManagement.Kafka
{
    public abstract class EventManager<TKey, TEvent> : BackgroundService, IEventManager where TEvent : ISpecificRecord
    {
        protected readonly IKafkaAccessor<TKey, TEvent> _kafkaAccessor;
        protected readonly ILogger<IKafkaAccessor<TKey, TEvent>> _logger;

        public EventManager(
            IKafkaAccessor<TKey, TEvent> kafkaAccessor,
            ILogger<IKafkaAccessor<TKey, TEvent>> logger
        )
        {
            _kafkaAccessor = kafkaAccessor;
            _logger = logger;
        }

        public abstract Task ProcessEvent();

        public abstract TEvent GetEvent(out long id);

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Yield();

            _logger.LogDebug("KafkaManager is started");

            stoppingToken.Register(() =>
            {
                _logger.LogDebug($"KafkaManager is stopping.");
                _kafkaAccessor.Consumed -= Run;
            });

            _kafkaAccessor.Consumed += Run;

            void Run(object kafkaAccessor, EventArgs args)
            {
                Task.Run(async () => await ProcessEvent(), _kafkaAccessor.CancellationTokenSource.Token);
            }
        }
    }
}
