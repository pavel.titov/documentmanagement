﻿using System.Threading.Tasks;

namespace DocumentManagement.Kafka
{
    public interface IEventManager
    {
        Task ProcessEvent();
    }
}