﻿using Avro.Specific;
using System;
using System.Threading.Tasks;

namespace DocumentManagement.Kafka
{
    public interface IKafkaProducer<TKey, TEvent> : IDisposable where TEvent : ISpecificRecord
    {
        Task ProduceEvent(string topic, TEvent cmd);
    }
}