﻿using Avro.Specific;
using Confluent.Kafka;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace DocumentManagement.Kafka
{
    public class KafkaProducerAdapter<TKey, TEvent> : IKafkaProducer<TKey, TEvent> where TEvent : ISpecificRecord
    {
        private readonly ILogger<IKafkaProducer<TKey, TEvent>> _logger;
        private readonly IProducer<TKey, TEvent> _producer;
        private bool disposedValue;

        public KafkaProducerAdapter(IProducer<TKey, TEvent> producer, ILogger<IKafkaProducer<TKey, TEvent>> logger)
        {
            _producer = producer;
            _logger = logger;
        }

        public async Task ProduceEvent(string topic, TEvent @event)
        {
            var deliveryResult = await _producer
                .ProduceAsync(topic, new Message<TKey, TEvent> { Value = @event });

            _logger.LogTrace("Sent event with offset {Offset} to {Topic}", deliveryResult.Offset.Value, topic);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _producer.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
