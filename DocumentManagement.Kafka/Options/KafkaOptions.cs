﻿using Confluent.Kafka;
using Confluent.SchemaRegistry;
using Confluent.SchemaRegistry.Serdes;

namespace DocumentManagement.Kafka
{
    public class KafkaOptions
    {
        public bool Enabled { get; set; } = true;
        public int Threads { get; set; }
        public string EventQueueTopic { get; set; }
        public string BootstrapServers { get; set; }
        public SchemaRegistryConfig SchemaRegistry { get; set; }
        public ConsumerConfig Consumer { get; set; }
        public ProducerConfig Producer { get; set; }
        public AvroSerializerConfig AvroSerializer { get; set; }
    }
}
