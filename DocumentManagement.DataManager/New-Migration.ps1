[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [string]
    $Name
)

dotnet ef migrations add $Name --startup-project ..\DocumentManagement.Api\DocumentManagement.Api.csproj