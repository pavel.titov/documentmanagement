﻿using DocumentManagement.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DocumentManagement.DataManager
{
    public class DataContext : DbContext, IDataContext
    {
        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentTransition> DocumentTransitions { get; set; }
        public DbSet<DocumentRoute> DocumentRoutes { get; set; }
        public DbSet<DocumentStage> DocumentStages { get; set; }
        public DbSet<DocumentTemplate> DocumentTemplates { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<User> Users { get; set; }

        public DataContext(DbContextOptions<DataContext> dbContextOptions) : base(dbContextOptions)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            HasFKeys(modelBuilder);

            new DataSeeder().Seed(modelBuilder);
        }

        private static void HasFKeys(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DocumentRoute>()
                .HasMany(p => p.Stages)
                .WithMany(p => p.Routes)
                .UsingEntity<DocumentRouteStage>(
                    j => j
                        .HasOne(pt => pt.Stage)
                        .WithMany(t => t.RouteStages)
                        .HasForeignKey(pt => pt.StageId),
                    j => j
                        .HasOne(pt => pt.Route)
                        .WithMany(p => p.RouteStages)
                        .HasForeignKey(pt => pt.RouteId),
                    j =>
                    {
                        j.HasKey(t => new { t.RouteId, t.StageId });
                    });
        }

        public async override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            var modified = ChangeTracker.Entries()
                .Where(t => t.State == EntityState.Modified)
                .Select(t => t.Entity);

            foreach (var a in modified)
            {
                ((IDateTrackable)a).DateEdit = DateTime.Now;
            }

            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
