﻿using DocumentManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace DocumentManagement.DataManager
{
    public interface IModelContext
    {
        DbSet<Document> Documents { get; set; }
        DbSet<DocumentTransition> DocumentTransitions { get; set; }
        DbSet<DocumentRoute> DocumentRoutes { get; set; }
        DbSet<DocumentStage> DocumentStages { get; set; }
        DbSet<DocumentTemplate> DocumentTemplates { get; set; }
        DbSet<DocumentType> DocumentTypes { get; set; }
        DbSet<User> Users { get; set; }
    }
}