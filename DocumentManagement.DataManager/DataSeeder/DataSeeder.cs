﻿using DocumentManagement.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace DocumentManagement.DataManager
{
    public class DataSeeder : IDataSeeder
    {
        public void Seed(ModelBuilder modelBuilder)
        {
            HasDocumentTypes(modelBuilder);
            HasDocumentRoutes(modelBuilder);
            HasDocumentStages(modelBuilder);
            HasDocumentRouteStages(modelBuilder);
            HasDocumentTemplates(modelBuilder);
            HasUsers(modelBuilder);
        }

        private static void HasDocumentTypes(ModelBuilder modelBuilder)
        {
            var documentTypes = new[]
            {
                new DocumentType() { Id = 1, Name = "Application", Description = "Заявление" },
                new DocumentType() { Id = 2, Name = "Certificate", Description = "Акт" },
                new DocumentType() { Id = 3, Name = "Order", Description = "Приказ" },
                new DocumentType() { Id = 4, Name = "Report", Description = "Отчет" },
            };
            foreach (var dt in documentTypes)
            {
                modelBuilder.Entity<DocumentType>().HasData(dt);
            }
        }

        private static void HasDocumentRoutes(ModelBuilder modelBuilder)
        {
            var documentRoutes = new[]
            {
                new DocumentRoute() { Id = 1 },
                new DocumentRoute() { Id = 2 },
            };
            foreach (var dr in documentRoutes)
            {
                modelBuilder.Entity<DocumentRoute>().HasData(dr);
            }
        }

        private static void HasDocumentStages(ModelBuilder modelBuilder)
        {
            var documentStages = new[]
            {
                new DocumentStage() { Id = (int)Stage.Draft,      Name = Enum.GetName(Stage.Draft),      Description = "Черновик" },
                new DocumentStage() { Id = (int)Stage.Registered, Name = Enum.GetName(Stage.Registered), Description = "Зарегистрирован" },
                new DocumentStage() { Id = (int)Stage.Pending,    Name = Enum.GetName(Stage.Pending),    Description = "На рассмотрении" },
                new DocumentStage() { Id = (int)Stage.Confirmed,  Name = Enum.GetName(Stage.Confirmed),  Description = "Согласован" },
                new DocumentStage() { Id = (int)Stage.Complete,   Name = Enum.GetName(Stage.Complete),   Description = "Исполнен" },
                new DocumentStage() { Id = (int)Stage.Canceled,   Name = Enum.GetName(Stage.Canceled),   Description = "Отменен" },
                new DocumentStage() { Id = (int)Stage.Archived,   Name = Enum.GetName(Stage.Archived),   Description = "В архиве" },
            };
            foreach (var ds in documentStages)
            {
                modelBuilder.Entity<DocumentStage>().HasData(ds);
            }
        }

        private static void HasDocumentRouteStages(ModelBuilder modelBuilder)
        {
            var documentRouteStages = new[]
            {
                new DocumentRouteStage() { Id = 1, RouteId = 1, StageId = (int)Stage.Draft },
                new DocumentRouteStage() { Id = 2, RouteId = 1, StageId = (int)Stage.Registered },
                new DocumentRouteStage() { Id = 3, RouteId = 1, StageId = (int)Stage.Pending },
                new DocumentRouteStage() { Id = 4, RouteId = 1, StageId = (int)Stage.Confirmed },
                new DocumentRouteStage() { Id = 5, RouteId = 1, StageId = (int)Stage.Complete },
                new DocumentRouteStage() { Id = 7, RouteId = 1, StageId = (int)Stage.Archived },

                new DocumentRouteStage() { Id = 1, RouteId = 2, StageId = (int)Stage.Draft },
                new DocumentRouteStage() { Id = 8, RouteId = 2, StageId = (int)Stage.Registered },
                new DocumentRouteStage() { Id = 9, RouteId = 2, StageId = (int)Stage.Complete },
                new DocumentRouteStage() { Id = 11, RouteId = 2, StageId = (int)Stage.Archived },
            };
            foreach (var drs in documentRouteStages)
            {
                modelBuilder.Entity<DocumentRouteStage>().HasData(drs);
            }
        }

        private static void HasDocumentTemplates(ModelBuilder modelBuilder)
        {
            var documentTemplates = new[]
            {
                new DocumentTemplate() { Id = 1, DocumentTypeId = 1, Name = "ApplicationJob", Description = "Заявление о приеме на работу", Template = "", RouteId = 1 },
                new DocumentTemplate() { Id = 2, DocumentTypeId = 1, Name = "ApplicationResignation", Description = "Заявление об увольнении", Template = "", RouteId = 1 },
                new DocumentTemplate() { Id = 3, DocumentTypeId = 1, Name = "ApplicationVacation", Description = "Заявление об отпуске", Template = "", RouteId = 1 },
                new DocumentTemplate() { Id = 4, DocumentTypeId = 3, Name = "OrderJob", Description = "Приказ о приеме на работу", Template = "", RouteId = 1 },
                new DocumentTemplate() { Id = 5, DocumentTypeId = 3, Name = "OrderResignation", Description = "Приказ об увольнении", Template = "", RouteId = 1 },
                new DocumentTemplate() { Id = 6, DocumentTypeId = 3, Name = "OrderVacation", Description = "Приказ об отпуске", Template = "", RouteId = 1 },
                new DocumentTemplate() { Id = 7, DocumentTypeId = 4, Name = "ReportMonthly", Description = "Ежемесячный отчет", Template = "", RouteId = 2 },
                new DocumentTemplate() { Id = 8, DocumentTypeId = 4, Name = "ReportYearly", Description = "Ежегодный отчет", Template = "", RouteId = 2 },
            };
            foreach (var dt in documentTemplates)
            {
                modelBuilder.Entity<DocumentTemplate>().HasData(dt);
            }
        }

        private static void HasUsers(ModelBuilder modelBuilder)
        {
            var users = new[]
            {
                new User() { Id = 1, Name = "Testov Test" },
                new User() { Id = 2, Name = "Ivanov Ivan" },
            };
            foreach (var u in users)
            {
                modelBuilder.Entity<User>().HasData(u);
            }
        }

    }
}
