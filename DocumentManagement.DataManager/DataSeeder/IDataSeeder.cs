﻿using Microsoft.EntityFrameworkCore;

namespace DocumentManagement.DataManager
{
    public interface IDataSeeder
    {
        void Seed(ModelBuilder modelBuilder);
    }
}