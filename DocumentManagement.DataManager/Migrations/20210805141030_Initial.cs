﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DocumentManagement.DataManager.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DocumentRoutes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateCreate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateEdit = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentRoutes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentStages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    DateCreate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateEdit = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentStages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    DateCreate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateEdit = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    DateCreate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateEdit = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentRouteStage",
                columns: table => new
                {
                    RouteId = table.Column<int>(type: "int", nullable: false),
                    StageId = table.Column<int>(type: "int", nullable: false),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateCreate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateEdit = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentRouteStage", x => new { x.RouteId, x.StageId });
                    table.ForeignKey(
                        name: "FK_DocumentRouteStage_DocumentRoutes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "DocumentRoutes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentRouteStage_DocumentStages_StageId",
                        column: x => x.StageId,
                        principalTable: "DocumentStages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DocumentTemplates",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DocumentTypeId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    Template = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RouteId = table.Column<int>(type: "int", nullable: false),
                    DateCreate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateEdit = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentTemplates_DocumentRoutes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "DocumentRoutes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentTemplates_DocumentTypes_DocumentTypeId",
                        column: x => x.DocumentTypeId,
                        principalTable: "DocumentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Documents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DocumentTemplateId = table.Column<int>(type: "int", nullable: false),
                    CreatorId = table.Column<int>(type: "int", nullable: false),
                    DateCreate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateEdit = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Documents_DocumentTemplates_DocumentTemplateId",
                        column: x => x.DocumentTemplateId,
                        principalTable: "DocumentTemplates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Documents_Users_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DocumentTransitions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DocumentId = table.Column<int>(type: "int", nullable: false),
                    StageId = table.Column<int>(type: "int", nullable: false),
                    DateCreate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateEdit = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentTransitions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentTransitions_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentTransitions_DocumentStages_StageId",
                        column: x => x.StageId,
                        principalTable: "DocumentStages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "DocumentRoutes",
                columns: new[] { "Id", "DateCreate", "DateEdit" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 5, 17, 10, 30, 300, DateTimeKind.Local).AddTicks(3433), null },
                    { 2, new DateTime(2021, 8, 5, 17, 10, 30, 300, DateTimeKind.Local).AddTicks(3441), null }
                });

            migrationBuilder.InsertData(
                table: "DocumentStages",
                columns: new[] { "Id", "DateCreate", "DateEdit", "Description", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 5, 17, 10, 30, 300, DateTimeKind.Local).AddTicks(6559), null, "Черновик", "Draft" },
                    { 2, new DateTime(2021, 8, 5, 17, 10, 30, 300, DateTimeKind.Local).AddTicks(7459), null, "Зарегистрирован", "Registered" },
                    { 3, new DateTime(2021, 8, 5, 17, 10, 30, 300, DateTimeKind.Local).AddTicks(7475), null, "На рассмотрении", "Pending" },
                    { 4, new DateTime(2021, 8, 5, 17, 10, 30, 300, DateTimeKind.Local).AddTicks(7479), null, "Согласован", "Confirmed" },
                    { 5, new DateTime(2021, 8, 5, 17, 10, 30, 300, DateTimeKind.Local).AddTicks(7482), null, "Исполнен", "Complete" },
                    { 6, new DateTime(2021, 8, 5, 17, 10, 30, 300, DateTimeKind.Local).AddTicks(7484), null, "Отменен", "Canceled" },
                    { 7, new DateTime(2021, 8, 5, 17, 10, 30, 300, DateTimeKind.Local).AddTicks(7487), null, "В архиве", "Archived" }
                });

            migrationBuilder.InsertData(
                table: "DocumentTypes",
                columns: new[] { "Id", "DateCreate", "DateEdit", "Description", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 5, 17, 10, 30, 298, DateTimeKind.Local).AddTicks(7610), null, "Заявление", "Application" },
                    { 2, new DateTime(2021, 8, 5, 17, 10, 30, 299, DateTimeKind.Local).AddTicks(5129), null, "Акт", "Certificate" },
                    { 3, new DateTime(2021, 8, 5, 17, 10, 30, 299, DateTimeKind.Local).AddTicks(5139), null, "Приказ", "Order" },
                    { 4, new DateTime(2021, 8, 5, 17, 10, 30, 299, DateTimeKind.Local).AddTicks(5141), null, "Отчет", "Report" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "DateCreate", "DateEdit", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(6998), null, "Testov Test" },
                    { 2, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(7184), null, "Ivanov Ivan" }
                });

            migrationBuilder.InsertData(
                table: "DocumentRouteStage",
                columns: new[] { "RouteId", "StageId", "DateCreate", "DateEdit", "Id" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(494), null, 1 },
                    { 2, 7, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(838), null, 11 },
                    { 2, 5, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(836), null, 9 },
                    { 1, 5, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(831), null, 5 },
                    { 1, 4, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(829), null, 4 },
                    { 1, 7, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(832), null, 7 },
                    { 2, 2, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(835), null, 8 },
                    { 1, 2, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(825), null, 2 },
                    { 2, 1, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(834), null, 1 },
                    { 1, 3, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(828), null, 3 }
                });

            migrationBuilder.InsertData(
                table: "DocumentTemplates",
                columns: new[] { "Id", "DateCreate", "DateEdit", "Description", "DocumentTypeId", "Name", "RouteId", "Template" },
                values: new object[,]
                {
                    { 7, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(5237), null, "Ежемесячный отчет", 4, "ReportMonthly", 2, "" },
                    { 1, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(4453), null, "Заявление о приеме на работу", 1, "ApplicationJob", 1, "" },
                    { 2, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(5193), null, "Заявление об увольнении", 1, "ApplicationResignation", 1, "" },
                    { 3, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(5198), null, "Заявление об отпуске", 1, "ApplicationVacation", 1, "" },
                    { 4, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(5232), null, "Приказ о приеме на работу", 3, "OrderJob", 1, "" },
                    { 5, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(5234), null, "Приказ об увольнении", 3, "OrderResignation", 1, "" },
                    { 6, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(5236), null, "Приказ об отпуске", 3, "OrderVacation", 1, "" },
                    { 8, new DateTime(2021, 8, 5, 17, 10, 30, 301, DateTimeKind.Local).AddTicks(5239), null, "Ежегодный отчет", 4, "ReportYearly", 2, "" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_DocumentRouteStage_StageId",
                table: "DocumentRouteStage",
                column: "StageId");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_CreatorId",
                table: "Documents",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_DocumentTemplateId",
                table: "Documents",
                column: "DocumentTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentTemplates_DocumentTypeId",
                table: "DocumentTemplates",
                column: "DocumentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentTemplates_RouteId",
                table: "DocumentTemplates",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentTransitions_DocumentId",
                table: "DocumentTransitions",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentTransitions_StageId",
                table: "DocumentTransitions",
                column: "StageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DocumentRouteStage");

            migrationBuilder.DropTable(
                name: "DocumentTransitions");

            migrationBuilder.DropTable(
                name: "Documents");

            migrationBuilder.DropTable(
                name: "DocumentStages");

            migrationBuilder.DropTable(
                name: "DocumentTemplates");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "DocumentRoutes");

            migrationBuilder.DropTable(
                name: "DocumentTypes");
        }
    }
}
