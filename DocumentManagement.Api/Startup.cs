using Confluent.Kafka;
using DocumentManagement.DataManager;
using DocumentManagement.Kafka;
using DocumentManagement.KafkaService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Text.Json.Serialization;

namespace DocumentManagement.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "DocumentManagement.Api", Version = "v1" });
            });
            services.AddDbContext<IDataContext, DataContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DbConnection"),
                    x => x.MigrationsAssembly(typeof(DataContext).Assembly.FullName)));

            services.Configure<KafkaOptions>(Configuration.GetSection("Kafka"));
            services.AddSingleton<ISchemaRegistry, SchemaRegistry>();
            services.AddSingleton(typeof(KafkaFactory<,>), typeof(KafkaDocumentTransitionFactory<,>));
            services.AddSingleton(sp =>
                sp.GetRequiredService<KafkaFactory<Null, DocumentTransitionEvent>>().CreateProducer()
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "DocumentManagement.Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
