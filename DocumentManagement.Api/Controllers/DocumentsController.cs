﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DocumentManagement.DataManager;
using DocumentManagement.Models;
using DocumentManagement.Kafka;
using Confluent.Kafka;
using DocumentManagement.KafkaService;
using Microsoft.Extensions.Options;

namespace DocumentManagement.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentsController : ControllerBase
    {
        private readonly IDataContext _context;
        private readonly IOptionsMonitor<KafkaOptions> _kafkaOptions;
        private readonly IKafkaProducer<Null, DocumentTransitionEvent> _producer;

        public DocumentsController(
            IDataContext context,
            IOptionsMonitor<KafkaOptions> kafkaOptions,
            IKafkaProducer<Null, DocumentTransitionEvent> producer
        )
        {
            _context = context;
            _kafkaOptions = kafkaOptions;
            _producer = producer;
        }

        // GET: api/Documents
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DocumentDto>>> GetDocuments()
        {
            return await _context.Documents
                .Include(d => d.DocumentTemplate)
                .Include(d => d.Creator)
                .Include(d => d.History)
                .ThenInclude(h => h.Stage)
                .Select(d => DocumentDto.FromDocument(d))
                .AsNoTracking()
                .ToListAsync();
        }

        // GET: api/Documents/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DocumentDto>> GetDocument(int id)
        {
            var document = await _context.Documents
                .Include(d => d.DocumentTemplate)
                .Include(d => d.Creator)
                .Include(d => d.History)
                .ThenInclude(h => h.Stage)
                .AsNoTracking()
                .FirstOrDefaultAsync(d => d.Id == id);

            if (document == null)
            {
                return NotFound();
            }

            return DocumentDto.FromDocument(document);
        }

        // POST: api/Documents
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<DocumentDto>> PostDocument(DocumentDto documentDto)
        {
            var document = new Document()
            {
                DocumentTemplateId = documentDto.TemplateId,
                CreatorId = 1
            };

            _context.Documents.Add(document);

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                await _context.SaveChangesAsync();

                var documentTransition = new DocumentTransition()
                {
                    DocumentId = document.Id,
                    StageId = (int)Stage.Draft
                };
                _context.DocumentTransitions.Add(documentTransition);

                await _context.SaveChangesAsync();

                await transaction.CommitAsync();
            }

            return CreatedAtAction("GetDocument", new { id = document.Id }, GetDocument(document.Id).Result.Value);
        }

        [HttpPost("{id}/transitionToNextStage")]
        public async Task<ActionResult<DocumentDto>> TransitionToNextStage(int id)
        {
            return await TransitionStage(id, DocumentTransitionAction.Next);
        }

        [HttpPost("{id}/transitionToPreviousStage")]
        public async Task<ActionResult<DocumentDto>> TransitionToPreviousStage(int id)
        {
            return await TransitionStage(id, DocumentTransitionAction.Previous);
        }

        [HttpPost("{id}/cancel")]
        public async Task<ActionResult<DocumentDto>> Cancel(int id)
        {
            return await TransitionStage(id, DocumentTransitionAction.Cancel);
        }

        private async Task<ActionResult<DocumentDto>> TransitionStage(int id, DocumentTransitionAction action)
        {
            var @event = new DocumentTransitionEvent()
            {
                DocumentId = id,
                EventAction = action
            };

            await _producer.ProduceEvent(_kafkaOptions.CurrentValue.EventQueueTopic, @event);

            return Ok();
        }
    }
}
