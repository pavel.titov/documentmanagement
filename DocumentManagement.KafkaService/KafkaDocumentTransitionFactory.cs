﻿using Avro.Specific;
using Confluent.Kafka;
using Confluent.Kafka.SyncOverAsync;
using Confluent.SchemaRegistry.Serdes;
using DocumentManagement.Kafka;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;

namespace DocumentManagement.KafkaService
{
    public class KafkaDocumentTransitionFactory<Null, DocumentTransitionEvent> : KafkaFactory<Null, DocumentTransitionEvent>
        where DocumentTransitionEvent : ISpecificRecord
    {
        private readonly IOptionsMonitor<KafkaOptions> _options;
        private readonly ISchemaRegistry _schemaRegistry;
        private readonly ILoggerFactory _loggerFactory;

        public KafkaDocumentTransitionFactory(
            IOptionsMonitor<KafkaOptions> options,
            ISchemaRegistry schemaRegistry,
            ILoggerFactory loggerFactory
        )
        {
            _options = options;
            _schemaRegistry = schemaRegistry;
            _loggerFactory = loggerFactory;
        }

        public override IKafkaConsumer<Null, DocumentTransitionEvent> CreateConsumer()
        {
            var consumerConfig = _options.CurrentValue.Consumer;
            consumerConfig.EnableAutoOffsetStore = false;

            Exception exception = null;

            var consumer =
                new ConsumerBuilder<Null, DocumentTransitionEvent>(consumerConfig)
                    .SetValueDeserializer(new AvroDeserializer<DocumentTransitionEvent>(_schemaRegistry.Client).AsSyncOverAsync())
                    .SetErrorHandler((c, e) =>
                    {
                        exception = new KafkaException(e);
                    })
                    .SetLogHandler((c, m) => { })
                    .Build();

            var consumerProxy = new KafkaConsumerAdapter<Null, DocumentTransitionEvent>(consumer, exception);

            return consumerProxy;
        }

        public override IKafkaProducer<Null, DocumentTransitionEvent> CreateProducer()
        {
            var producerConfig = _options.CurrentValue.Producer;

            var avroSerializerConfig = _options.CurrentValue.AvroSerializer;

            var logger = _loggerFactory.CreateLogger<KafkaProducerAdapter<Null, DocumentTransitionEvent>>();

            var producer =
                new ProducerBuilder<Null, DocumentTransitionEvent>(producerConfig)
                    .SetValueSerializer(new AvroSerializer<DocumentTransitionEvent>(_schemaRegistry.Client, avroSerializerConfig))
                    .SetErrorHandler((p, e) => logger.LogWarning("Kafka producer error: {Reason}", e.Reason))
                    .SetLogHandler((p, m) => { })
                    .Build();

            return new KafkaProducerAdapter<Null, DocumentTransitionEvent>(producer, logger);
        }
    }
}
