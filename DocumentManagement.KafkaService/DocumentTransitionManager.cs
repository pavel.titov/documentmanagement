﻿using Confluent.Kafka;
using DocumentManagement.DataManager;
using DocumentManagement.Kafka;
using DocumentManagement.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DocumentManagement.KafkaService
{
    public class DocumentTransitionManager : EventManager<Null, DocumentTransitionEvent>
    {
        private readonly object _lockGetNextEvent = new();
        private ManualResetEventSlim _previousEventLock = new(true);

        private readonly IServiceScope _scope;
        private readonly IDataContext _dataContext;

        public DocumentTransitionManager(
            IKafkaAccessor<Null, DocumentTransitionEvent> kafkaAccessor,
            ILogger<IKafkaAccessor<Null, DocumentTransitionEvent>> logger,
            IServiceProvider serviceProvider
        ) : base(kafkaAccessor, logger)
        {
            _scope = serviceProvider.CreateScope();
            _dataContext = _scope.ServiceProvider.GetService<IDataContext>();
        }

        public override async Task ProcessEvent()
        {
            var @event = GetEvent(out long id);

            @event.PreviousEventLock.Wait();

            await TransitionDocument(@event);
            _kafkaAccessor.UpdateStatus(id);

            @event.Lock.Set();
        }

        public override DocumentTransitionEvent GetEvent(out long id)
        {
            DocumentTransitionEvent @event;

            lock (_lockGetNextEvent)
            {
                @event = _kafkaAccessor.GetEvent(out id);

                @event.PreviousEventLock = _previousEventLock;
                @event.Lock = new ManualResetEventSlim(false);
                _previousEventLock = @event.Lock;

                _logger.LogDebug("Processing event with offset {offset}", id);
            }
            return @event;
        }

        private async Task TransitionDocument(DocumentTransitionEvent documentEvent)
        {
            var document = await _dataContext.Documents
                .Include(d => d.DocumentTemplate)
                .ThenInclude(dt => dt.Route)
                .ThenInclude(r => r.Stages)
                .Include(d => d.History)
                .ThenInclude(h => h.Stage)
                .FirstOrDefaultAsync(d => d.Id == documentEvent.DocumentId);

            var currentTransition = document.History.OrderByDescending(h => h.Id).First();

            if (currentTransition.StageId == (int)Stage.Canceled)
            {
                return;
            }

            int? stageId = null;

            if (documentEvent.EventAction == DocumentTransitionAction.Cancel)
            {
                stageId = (int)Stage.Canceled;
            }
            else
            {
                var route = document.DocumentTemplate.Route;
                var routeStages = route.Stages.ToList();

                int iCurrentStage = routeStages.FindIndex(s => s.Id == currentTransition.StageId);

                int? iNewStage = null;
                switch (documentEvent.EventAction)
                {
                    case DocumentTransitionAction.Next:
                        iNewStage = iCurrentStage + 1;
                        break;
                    case DocumentTransitionAction.Previous:
                        iNewStage = iCurrentStage - 1;
                        break;
                }

                if (iNewStage >= routeStages.Count || iNewStage < 0)
                {
                    return;
                }

                var newStage = routeStages[(int)iNewStage];
                stageId = newStage.Id;
            }

            _dataContext.DocumentTransitions.Add(new DocumentTransition()
            {
                DocumentId = documentEvent.DocumentId,
                StageId = (int)stageId,
            });

            await _dataContext.SaveChangesAsync();
        }
    }
}
