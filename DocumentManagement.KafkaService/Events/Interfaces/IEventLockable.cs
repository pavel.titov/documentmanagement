﻿using System.Threading;

namespace DocumentManagement.KafkaService
{
    public interface IEventLockable
    {
        ManualResetEventSlim Lock { get; set; }
        ManualResetEventSlim PreviousEventLock { get; set; }
    }
}