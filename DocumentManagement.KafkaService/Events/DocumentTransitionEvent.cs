﻿using System.Threading;

namespace DocumentManagement.KafkaService
{
    public partial class DocumentTransitionEvent : IEventLockable
    {
        public ManualResetEventSlim PreviousEventLock { get; set; } = new(true);
        public ManualResetEventSlim Lock { get; set; } = new(false);
    }
}
